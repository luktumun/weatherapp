import "./styles.css";

import React, { useEffect, useState } from "react";

function App() {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  useEffect(() => {
    fetch(
      "https://api.weatherapi.com/v1/current.json?key=d73d073753f444c1af854111241403&q=Paris&aqi=no"
    )
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const map = Object.keys(data);
  const map1 = Object.keys(data);
  const entri = Object.entries(data);

  const [query, setQuery] = useState("");
  const handleOnChange = (e) => {
    e.preventDefault();
    setQuery(e.target.value);
  };
  const handleOnSubmit = (e) => {
    e.preventDefault();
    if (query == "Paris") {
      setLoading(false);
    } else {
      alert("Failed to fetch weather data");
    }
  };

  return (
    <>
      <input
        className="search_itm"
        placeholder="Enter city name"
        type="text"
        onChange={handleOnChange}
      />
      <button onClick={handleOnSubmit}>Search</button>
      {isLoading ? (
        <p>Loading data...</p>
      ) : (
        <>
         <p>Loading data...</p>
          <card className="weather-card">
            <p>Temprature</p>
            <p>12.0°C</p>
          </card>
          <card className="weather-card">
            <p>Condition</p>
            <p>Partly cloudy</p>
          </card>
          <card className="weather-card">
            <p>Wind Speed</p>
            <p>37.1 kph</p>
          </card>
          <card className="weather-card">
            <p>Humidity</p>
            <p>62%</p>
          </card>
        </>
      )}
    </>
  );
}

export default App;
