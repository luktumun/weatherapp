import React from "react";
import { useState, useEffect } from "react";
import { Card } from "semantic-ui-react";

import "./Xweather.css";

const Xweather = () => {
  const [data, setData] = useState([]);

  const [isLoading, setLoading] = useState(true);

  const [searchCity, setSearchCity] = useState("");

  useEffect(() => {
    if (searchCity > 1) {
      fetch(
        `https://api.weatherapi.com/v1/current.json?key=d73d073753f444c1af854111241403&q=${searchCity}&aqi=no`
      )
        .then((res) => res.json())
        .then((data) => {
          setData(data);
        });
    }
  }, [searchCity]);

  const handleOnChange = (e) => {
    e.preventDefault();
    setSearchCity(e.target.value);
  };

  const handleOnSubmit = (e) => {
    if (data) {
      setLoading(false);
    } else {
      alert("Failed to fetch data");
    }
  };

  return (
    <>
      <input
        className="search_itm"
        placeholder="Enter City Name"
        type="text"
        onChange={(e) => handleOnChange(e)}
      />
      <button onClick={(e) => handleOnSubmit(e)}>Search</button>
      {isLoading ? (
        <p>Loading data...</p>
      ) : (
        <>
          <Card className="weather_card">
            <h1>Temperature</h1>
            <p>{data.current.temp_c}°C</p>
          </Card>
          <Card className="weather_card">
            <h1>Humidity</h1>
            <p>{data.current.humidity}%</p>
          </Card>
          <Card className="weather_card">
            <h1>Condition</h1>
            <p>{data.current.condition.text}</p>
          </Card>
          <Card className="weather_card">
            <h1>Wind Speed</h1>
            <p>{data.current.wind_kph} km/h</p>
          </Card>
        </>
      )}
    </>
  );
};
export default Xweather;
